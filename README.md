# FreeOPCUAClientDashboard

This is an OPCUAClient dashboard flask solution

To use this you need to:
* Install python. 
*    https://www.python.org/downloads/
* Install Flask. 
     https://flask.palletsprojects.com/en/1.1.x/
* Install FreeOPCUa 
     https://github.com/FreeOpcUa/freeopcua
     check this video https://www.youtube.com/watch?v=mEbPHflLNyc
* Copy this proyect to one folder.
* Modify the nodes.json file in static folder "/static/nodes.json"
     in the file you can se a getnode list, here is the configuration to every node of the server module, for example

               {
					"name":"temperature",
					"node":"ns=2;i=2",
					"chartType":"bar",
					"xAxes":"Time",
					"yAxes":"°C",
					"title":"Temperature of the system",
					"backgroundColor":"rgb(255, 99, 132)",
					"borderColor":"rgb(255, 99, 132)",
					"datasetLabel":"Meas"
				}
				
         name: apply to the container in the html code.
         node: is the string of the conection to the node.
         chartType: this can be line, bar, pie, radar, this follow the chart.js framework, and added is "Text" to show only text.
         xAxes: is the x Axes title.
         yAxes: is the y Axes title.
         title: is the general title.
         backgroundColor: This is the background color, requiered for chart.js parameters.
         borderColor: This is the border Color, requiered for chart.js parameters.
         datasetLabel: This is the title of the dataset.
         
         For the view, there is the index.html
         Here you can use bootstrap
         the container is a canvas:
         <canvas id="canvas"></canvas>
         
         The canvas Id need to be the same as getnode[x].name field in the nodes.json file, here will be built the chart
         then you need to set the /static/nodes.json file and the view in the index.html with the canvas container needs
         
         IF YOU USE THE OPCUASimulator.py PLEASE DON'T CHANGE THE nodes.json file, currently is configured to run with simulator
         
* In windows, using cmd, goto the folder, in the root type the next "set FLASK_APP=dashboard.py"
* Now to run type the next "flask run"
 



This follow the freeopua LGPL licence.
Thanks