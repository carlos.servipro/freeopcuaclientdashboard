#from flask import Flask
#from flask import render_template, redirect, url_for
#app = Flask(__name__)

#@app.route('/')
##def hello_world():
    ##return 'Hello, World!'
    
#def index():
    #return render_template('index.html')
    
import json
import random
import time
from datetime import datetime
import opcua
from opcua import ua, uamethod
import time
import os
from flask import request, jsonify #import jsonify

from flask import Flask, Response, render_template

application = Flask(__name__)
random.seed()  # Initialize the random number generator
OPCUAServer="";
client=None;

@application.route('/index')
def index():
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
        print(json_data)
    serverJsonData=json_data["CustomDashboard"][0]["getnode"]
    global OPCUAServer
    global client
    OPCUAServer = json_data["nodes"][0]["server"]
    print("Server")
    print(OPCUAServer)
    client= opcua.Client(OPCUAServer)
    client.connect()
    return render_template('index.html',len=len(serverJsonData),serverJsonData=serverJsonData)
    
@application.route('/')
def menu():
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    data=json_data["ViewByPorts"]
    return render_template('menu.html',len=len(data),serverJsonData=data)

@application.route('/customdashboard')
def customdashboard():
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    data=json_data["CustomDashboard"]
    return render_template('customdashboard.html',len=len(data),serverJsonData=data)
	
@application.route('/portview')
def portview():
    global OPCUAServer
    global client
    dataKey=None
    if request.method == 'GET':
        dataKey = request.args.get('dataKey')
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    OPCUAServer = json_data["ViewByPorts"][dataKey]["server"]
    client= opcua.Client(OPCUAServer)
    client.connect()		
    data=json_data["ViewByPorts"][dataKey]
    return render_template('portview.html',serverJsonData=data)

@application.route('/dashboardview')
def dashboardview():
    global OPCUAServer
    global client
    dataKey=None
    if request.method == 'GET':
        dataKey = request.args.get('dataKey')
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    OPCUAServer = json_data["CustomDashboard"][dataKey]["server"]
    client= opcua.Client(OPCUAServer)
    client.connect()
    data=json_data["CustomDashboard"][dataKey]["data"]
    template=json_data["CustomDashboard"][dataKey]["template"]
    return render_template(template,serverJsonData=data)
    
@application.route('/rfIdport')
def rfIdport():
    global OPCUAServer
    global client
    dataKey=None
    if request.method == 'GET':
        dataKey = request.args.get('dataKey')
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    OPCUAServer = json_data["ViewByPorts"][dataKey]["server"]
    client= opcua.Client(OPCUAServer)
    client.connect()
    data=json_data["ViewByPorts"][dataKey]
    return render_template('rfIdport.html',len=len(data),serverJsonData=data)	

@application.route('/chart-data')
def chart_data():
    def generate_random_data():
        while True:
            json_data = json.dumps(
                {'time': datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'value': random.random() * 100})
            yield f"data:{json_data}\n\n"
            time.sleep(0.5)

    return Response(generate_random_data(), mimetype='text/event-stream')
    
@application.route('/OPCUAClient')
def OPCUAClient():
    global client
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    def read_data(node):
        while True:
            Port1=client.get_node(node)
            SensorVal=Port1.get_value()
            json_data = json.dumps(
                    {'time': datetime.now().strftime('%H:%M:%S'), 'value': SensorVal})
            yield f"data:{json_data}\n\n"
            time.sleep(0.5)
    node=None
    if request.method == 'GET':
        node = request.args.get('node')

    if node != None:
        return Response(read_data(node), mimetype='text/event-stream')
    else:
        return Response("Plese send the node", mimetype='text/event-stream')

@application.route('/OPCUAClient2')
def OPCUAClient2():
    global client
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    def read_data(node1):
        while True:
            Port1=client.get_node(node1)
            SensorVal1=Port1.get_value()
            if type(SensorVal1)==bytes:
                SensorVal1=''.join('{:02x}'.format(x) for x in SensorVal1)
            json_data = json.dumps(
                    {'time': datetime.now().strftime('%H:%M:%S'), 'value': SensorVal1})
            yield f"data:{json_data}\n\n"
            time.sleep(0.5)
    node1=None
    if request.method == 'GET':
        node1 = request.args.get('node1')

    if node1 != None:
        return Response(read_data(node1), mimetype='text/event-stream')
    else:
        return Response("Plese send the node", mimetype='text/event-stream')
		
@application.route('/CALLMethod')
def CALLMethod():
    global client
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    def read_data(vnode,vmethod,vinputs,v1,v2,v3):
        parent = client.get_node(vnode)
        method = parent.get_child(vmethod)
        inputs = method.get_child(vinputs).get_value()
        arguments = [ua.Variant(int(v1),ua.VariantType.UInt32),ua.Variant(int(v2),ua.VariantType.UInt32),ua.Variant(int(v3),ua.VariantType.UInt32)]
        res = parent.call_method(method, *arguments)
        json_data = json.dumps(
				{'time': datetime.now().strftime('%H:%M:%S'), 'value': res})
        yield f"data:{json_data}\n\n"
    vnode:None
    vmethod:None
    vinputs:None
    v1=None
    v2=None
    v3=None
    if request.method == 'GET':
        vnode = request.args.get('node')
        vmethod = request.args.get('method')
        vinputs = request.args.get('inputs')
        v1 = request.args.get('v1')
        v2 = request.args.get('v2')
        v3 = request.args.get('v3')
        print(v1)

    if v1 != None:
        return Response(read_data(vnode,vmethod,vinputs,v1,v2,v3), mimetype='text/event-stream')
    else:
        return Response("Plese send the v1,v2 and v3", mimetype='text/event-stream')

@application.route('/WriteDataTag')
def WriteDataTag():
    global client
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    def read_data(vnode,value):
        parent = client.get_node(vnode)
        ds=b'\x01\x00\x00\x00'
        rd=b'\x01\x00\x00\x00'
        wr=b'\x02\x00\x00\x00'
        dsData=ds+value.encode()
        wrData=wr+value.encode()
        rdData=rd+value.encode()
        parent.set_value(rdData)
        time.sleep(1)
        parent.set_value(wrData)
        time.sleep(1)
        parent.set_value(rdData)
        json_data = json.dumps(
				{'time': datetime.now().strftime('%H:%M:%S'), 'value': 'Succesful'})
        yield f"data:{json_data}\n\n"
    vnode:None
    v1=None
    if request.method == 'GET':
        vnode = request.args.get('node')
        v1 = request.args.get('v1')
    if v1 != None:
        return Response(read_data(vnode,v1), mimetype='text/event-stream')
    else:
        return Response("Plese send the v1", mimetype='text/event-stream')
		
@application.route('/ReadDataTag')
def ReadDataTag():
    global client
    filename = os.path.join(application.static_folder, 'nodes.json')
    with open(filename) as blog_file:
        json_data = json.load(blog_file)
    def read_data(vnode,value):
        parent = client.get_node(vnode)
        cmd=b'\x00\x00\x00\x00'
        if int(value)==1:
            cmd=b'\x01\x00\x00\x00'
        parent.set_value(cmd)
        time.sleep(1)
        json_data = json.dumps(
				{'time': datetime.now().strftime('%H:%M:%S'), 'value': 'Succesful'})
        yield f"data:{json_data}\n\n"
    vnode:None
    v1=None
    if request.method == 'GET':
        vnode = request.args.get('node')
        v1 = request.args.get('v1')
    if v1 != None:
        return Response(read_data(vnode,v1), mimetype='text/event-stream')
    else:
        return Response("Plese send the v1", mimetype='text/event-stream')	
		
if __name__ == '__main__':
    application.run(host='0.0.0.0',debug=True, threaded=True)
