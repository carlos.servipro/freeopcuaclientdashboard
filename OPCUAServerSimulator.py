from opcua import Server
from random import randint
import time

server = Server()

url = "opc.tcp://192.168.1.35:4840"
server.set_endpoint(url)

name = "OPCUA_SIMULATOR_SERVER"
addspace = server.register_namespace(name)

node = server.get_objects_node()

Param = node.add_object(addspace,"Parameters")

Temp = Param.add_variable(addspace,"Temperature",0)
Volt = Param.add_variable(addspace,"Voltage",0)
Amp = Param.add_variable(addspace,"Current",0)
Pres = Param.add_variable(addspace,"Pressure",0)



Temp.set_writable()
Volt.set_writable()
Amp.set_writable()
Pres.set_writable()

server.start()

print("Server starter at {}".format(url))

while True:
    Temperature = randint(10,50)
    Voltage = randint(10,50)
    Current = randint(10,50)
    Pressure = randint(10,50)
    #print(Temperature)

    Temp.set_value(Temperature)
    Volt.set_value(Voltage)
    Amp.set_value(Current)
    Pres.set_value(Pressure)
    time.sleep(2)
